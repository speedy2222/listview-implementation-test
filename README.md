
In the project are 2 activities, 'MainActivity' which is acting as launcher app and 'DetailActivity'.

MainActivity present list of items (photos) and DetailActivity presents detailed/full size photo with text.

To get the server data you will use VolleyProject and its implementation is can be found in the network package.

Your task is get data from 'PhotosManager' into 'MainActivity' and then present them as list of photos.

We have prepared the item layout to be used in list. (item_layout.xml). What is also necessary, is to load images into the item using the url given by Photo object.

When a user taps any item on the list, it opens DetailActivity where it will shown large image with text.

For loading the images, you can use your preferred image library.