package test.mirror.com.testapplication.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import test.mirror.com.testapplication.R;
import test.mirror.com.testapplication.model.Photo;

public class DetailActivity extends AppCompatActivity {

    public static final String PHOTO_TAG = "photo_tag";
    private TextView titleText;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        titleText = (TextView) findViewById(R.id.photo_title_TextView);
        imageView = (ImageView) findViewById(R.id.photo_detail_ImageView);

        Photo photo = getIntent().getParcelableExtra(PHOTO_TAG);

        setDetail(photo);
    }


    public void setDetail(Photo photo) {
        titleText.setText(photo.getTitle());
        //TODO implement image loading

    }
}
