package test.mirror.com.testapplication.network;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.List;

import test.mirror.com.testapplication.model.Photo;

/**
 * Created by robertslama on 08/03/2016.
 */
public class PhotosManager<T extends Context & PhotosManager.Callback> implements Response.Listener<List<Photo>>, Response.ErrorListener {

    public interface Callback {
        void onErrorResponse(String error);

        void onSuccessResponse(List<Photo> photos);
    }

    private final RequestQueue requestQueue;
    private final T t;

    public PhotosManager(T context) {
        requestQueue = Volley.newRequestQueue(context);
        t = context;
    }

    public void requestPhotoList() {
        PhotosRequest request = new PhotosRequest(NetworkConfig.PHOTO_URL, this, this);
        requestQueue.add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        t.onErrorResponse(error.getMessage());
    }

    @Override
    public void onResponse(List<Photo> response) {
        t.onSuccessResponse(response);
    }
}
