package test.mirror.com.testapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by robertslama on 08/03/2016.
 */
public class Photo implements Parcelable {

    @SerializedName("albumId")
    private String albumId;
    @SerializedName("id")
    private String photoId;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String photoUrl;
    @SerializedName("thumbnailUrl")
    private String thumbnailUrl;

    public Photo() {
    }

    public Photo(Parcel source) {
        albumId = source.readString();
        photoId = source.readString();
        title = source.readString();
        photoUrl = source.readString();
        thumbnailUrl = source.readString();
    }


    //================= PARCELABLE ===================
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(albumId);
        dest.writeString(photoId);
        dest.writeString(title);
        dest.writeString(photoUrl);
        dest.writeString(thumbnailUrl);
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    //==================================================

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "albumId='" + albumId + '\'' +
                ", photoId='" + photoId + '\'' +
                ", title='" + title + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                '}';
    }


}
