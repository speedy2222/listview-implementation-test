package test.mirror.com.testapplication.parsers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import test.mirror.com.testapplication.model.Photo;

/**
 * Created by robertslama on 08/03/2016.
 */
public class PhotoParser {

    private final Gson gson;

    public PhotoParser() {
        gson = new GsonBuilder().create();
    }

    public List<Photo> parsePhotoList(byte[] data) throws UnsupportedEncodingException {
        final List<Photo> result = new LinkedList<Photo>();
        JsonArray serverJsonResponse = new JsonParser().parse(new String(data, "UTF-8")).getAsJsonArray();

        Type listType = new TypeToken<List<Photo>>() {
        }.getType();

        List<Photo> photos = gson.fromJson(serverJsonResponse, listType);

        if (photos != null && photos.size() > 0) {
            result.addAll(photos);
        }

        return result;
    }
}
