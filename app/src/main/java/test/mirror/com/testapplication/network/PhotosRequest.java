package test.mirror.com.testapplication.network;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.List;

import test.mirror.com.testapplication.model.Photo;
import test.mirror.com.testapplication.parsers.PhotoParser;

/**
 * Created by robertslama on 08/03/2016.
 */
public class PhotosRequest extends Request<List<Photo>> {

    private static final String TAG = PhotosRequest.class.getSimpleName();
    private final Response.Listener responseListener;

    public PhotosRequest(String url, Response.Listener responseListener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.responseListener = responseListener;
    }

    @Override
    protected Response<List<Photo>> parseNetworkResponse(NetworkResponse networkResponse) {
        if (networkResponse == null
                || networkResponse.data == null
                || networkResponse.data.length == 0) {
            return Response.error(new VolleyError("Empty response!"));
        }

        PhotoParser photosParser = new PhotoParser();
        List<Photo> photos = null;
        try {
            photos = photosParser.parsePhotoList(networkResponse.data);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "UnsupportedEncodingException: " + e.getMessage());
            return Response.error(new VolleyError(e.getMessage()));
        }

        return Response.success(photos, HttpHeaderParser.parseCacheHeaders(networkResponse));
    }

    @Override
    protected void deliverResponse(List<Photo> response) {
        if (responseListener != null) {
            responseListener.onResponse(response);
        }
    }
}
