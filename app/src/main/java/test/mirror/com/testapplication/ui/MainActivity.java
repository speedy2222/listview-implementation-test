package test.mirror.com.testapplication.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import java.util.List;

import test.mirror.com.testapplication.R;
import test.mirror.com.testapplication.model.Photo;
import test.mirror.com.testapplication.network.PhotosManager;

public class MainActivity extends AppCompatActivity implements PhotosManager.Callback {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listview);

        //request to get list of photos (5000 items)
        PhotosManager<MainActivity> photosManager = new PhotosManager<MainActivity>(this);
        photosManager.requestPhotoList();
    }

    @Override
    public void onErrorResponse(String error) {
        Log.e(TAG, error);
    }

    @Override
    public void onSuccessResponse(List<Photo> photos) {
        //TODO implement fill listview with received data
        Log.d(TAG, "photos list.size(" + (photos != null ? photos.size() : 0) + ")");
    }

}
